<?php

namespace App\Controller;

use App\Entity\Cart;
use App\Entity\CustomProduct;
use App\Entity\Product;
use App\Entity\RequestedProduct;
use App\Entity\StandardProduct;
use App\Entity\User;
use App\Repository\CartRepository;
use App\Repository\ProductRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class CartController extends AbstractController
{
    private $em;

    /**
     * @Route("/api/cart", name="cart")
     */
    public function index(): Response
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/CartController.php',

        ]);
    }

    /**
     * @param $user_id
     * @Route("/api/cartListByUserId/{user_id}", name="cartListByUserId", methods={"GET"})
     */
    public function ReadUsercart($user_id): Response
    {
        $theCart = $this->getDoctrine()
            ->getRepository(cart::class)
            ->findBy(
                ['user' => $user_id,
                    'active' => 1]);
        $encoders = array(new JsonEncoder());
        $serializer = new Serializer([new ObjectNormalizer()], $encoders);
        $data = $serializer->serialize($theCart, 'json');
        $response = new Response($data, 200);
        //content type
        $response->headers->set('Content-Type', 'application/json');
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }
    /**
     * @Route("/api/cartList", name="cartList", methods={"GET"})
     */
    public  function getAllcarts(CartRepository $repository): Response
    {
        $list = $repository->findAll();
        $encoders = array(new JsonEncoder());
        $serializer = new Serializer([new ObjectNormalizer()], $encoders);
        $data = $serializer->serialize($list, 'json');
        $response = new Response($data, 200);
        //content type
        $response->headers->set('Content-Type', 'application/json');
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }

    /**
     * @param $user_id
     * @param $product_id
     * @Route("/api/deleteProductFromCart/{user_id}/{product_id}", name="api_delete", methods={"DELETE"})
     * @return Response
     *
     */
    public function deleteProductCart($user_id, $product_id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $product_ = $em->getRepository(cart::class)->findOneBy(
            ['user' => $user_id,
            'product' => $product_id]);

            $em->remove($product_);

        $em->flush();
        $response = new Response('', Response::HTTP_OK);
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'DELETE');
        return $response;
    }

    /**
     * @param $user_id
     * @param $product_id
     * @param $product_type
     * @Route("/api/addProductToCart/{product_id}/productType/{product_type}/ForUser/{user_id}", name="addProductToCart", methods={"post"})
     * @return Response
     */
    public function addProductToCart($product_id, $product_type, $user_id) : Response
    {

            //get user by id
            $em = $this->getDoctrine()->getManager();
            $user_ = $em->getRepository(user::class)->find($user_id);
            //get product by id
            if($product_type == "Sp")
            {
                $e = $this->getDoctrine()->getManager();
                $product_ = $e->getRepository(StandardProduct::class)->find($product_id);
            }elseif ($product_type == "Cp"){

                $e = $this->getDoctrine()->getManager();
                $product_ = $e->getRepository(CustomProduct::class)->find($product_id);
            }else{
                $e = $this->getDoctrine()->getManager();
                $product_ = $e->getRepository(RequestedProduct::class)->find($product_id);
            }

            //get cart with the same user and product
            $emc = $this->getDoctrine()->getManager();
            $cart_ = $emc->getRepository(cart::class)->findOneBy(
            ['user' => $user_id,
                'product' => $product_id,
                'active' => 1]);
            //if the product already exists in the cart
            if($cart_)
            {
                $cart_->setQte($cart_->getQte()+1);
                $cart_->setTotalPrice($cart_->getTotalPrice()+$product_->getPrice());
                $emc->flush();

            }else{
                $cart = new Cart();
                $cart->setUser($user_);
                $cart->setActive("1");
                $cart->setQte("1");
                $cart->setTotalPrice($product_->getPrice());
                $cart->setProduct($product_);

                //add the cart to database
                $em = $this->getDoctrine()->getManager();
                $em->persist($cart);
                $em->flush();
            }

            $response = new Response('', Response::HTTP_CREATED);
            //Allow all websites
            $response->headers->set('Access-Control-Allow-Origin', '*');
            // You can set the allowed methods too, if you want
            $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');


        return $response;
    }

    /**
     * @param $product_price
     * @Route("/api/UpdateQuantityProduct/{id}/{product_price}", name="UpdateQuantityProduct", methods={"POST"})
     */
    public function UpdateQuantityProduct(Request $request,$id,$product_price): Response
    {



        $entityManager = $this->getDoctrine()->getManager();
        $object = $entityManager->getRepository(Cart::class)->find($id);

        $data = json_decode($request->getContent(), true);
        $object->setQte($data["qte"]);

        $object->setTotalPrice($object->getQte() * $product_price);
        $entityManager->flush();


        $response = new Response('', Response::HTTP_OK);
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;

    }



}
