<?php

namespace App\Controller;

use App\Entity\Cart;
use App\Entity\CustomProduct;
use App\Entity\Order;
use App\Entity\RequestedProduct;
use App\Entity\StandardProduct;
use App\Entity\User;
use App\Repository\OrderRepository;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class OrderController extends AbstractController
{
    /**
     * @Route("/api/order", name="order")
     */
    public function index(): Response
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/OrderController.php',
        ]);
    }


    //client
    /**
     * @param $user_id
     * @Route("/api/submitCard/{user_id}/{dueDate}", name="submitCard", methods={"post"})
     * @return Response
     */
    public function submitCard($user_id,$dueDate) : Response
    {

        //get cart with the same user and product
        $emc = $this->getDoctrine()->getManager();
        $cart = $emc->getRepository(cart::class)->findBy(
            ['user' => $user_id, 'active' => 1]);

        if ($cart)
        {
            //totalprice of the order
            $totalPrice = 0;
            foreach ($cart as $c){
                $totalPrice = $totalPrice + $c->getTotalPrice();
            }
            //qte of products in the order
            $qte = 0;
            foreach ($cart as $c){
                $qte = $qte + $c->getQte();
            }

            $order = new Order();
            $order->setTotalPrice($totalPrice);
            $order->setQte($qte);
            $order->setDueDate(new \DateTime($dueDate));
            $order->setCreationDate(new \DateTime('now'));
            $order->setStatus("waiting for validation");

            //set active cart to 0
            foreach ($cart as $c){
                $c->setActive(0);
                $em = $this->getDoctrine()->getManager();
                $em->persist($c);
                $em->flush();
            }


            //add the order to database
            $em = $this->getDoctrine()->getManager();
            $em->persist($order);
            $em->flush();

        }
        $response = new Response('', Response::HTTP_CREATED);
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');


        return $response;
    }

    //Admin

    /**
     * @param $id
     * @Route("/api/acceptOrder/{id}", name="acceptOrder", methods={"post"})
     * @return Response
     */
    public function acceptOrder($id) : Response
    {

        //get cart with the same user and product
        $emc = $this->getDoctrine()->getManager();
        $order = $emc->getRepository(order::class)->find($id);

        $order->setStatus("in progress");
        $em = $this->getDoctrine()->getManager();
        $em->persist($order);
        $em->flush();



        $response = new Response('', Response::HTTP_CREATED);
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');


        return $response;
    }

    /**
     * @param $id
     * @Route("/api/rejectOrder/{id}", name="rejectOrder", methods={"post"})
     * @return Response
     */
    public function rejectOrder($id) : Response
    {

        //get cart with the same user and product
        $emc = $this->getDoctrine()->getManager();
        $order = $emc->getRepository(order::class)->find($id);

        $order->setStatus("rejected");
        $em = $this->getDoctrine()->getManager();
        $em->persist($order);
        $em->flush();



        $response = new Response('', Response::HTTP_CREATED);
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');


        return $response;
    }

    /**
     * @Route("/api/orderList", name="orderList", methods={"GET"})
     */
    public  function getAllorders(OrderRepository $repository): Response
    {
        $list = $repository->findAll();
        $encoders = array(new JsonEncoder());
        $serializer = new Serializer([new ObjectNormalizer()], $encoders);
        $data = $serializer->serialize($list, 'json');
        $response = new Response($data, 200);
        //content type
        $response->headers->set('Content-Type', 'application/json');
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }

    /**
     * @Route("/api/getInProgressOrders", name="getInProgressOrders", methods={"GET"})
     */
    public function getInProgressOrders(): Response
    {
        $theorders = $this->getDoctrine()
            ->getRepository(order::class)
            ->findBy(
                ['status' => "in progress"]);
        $encoders = array(new JsonEncoder());
        $serializer = new Serializer([new ObjectNormalizer()], $encoders);
        $data = $serializer->serialize($theorders, 'json');
        $response = new Response($data, 200);
        //content type
        $response->headers->set('Content-Type', 'application/json');
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }


}
