<?php

namespace App\Controller;

use App\Entity\CustomProduct;
use App\Entity\Filling;
use App\Entity\RequestedProduct;
use App\Entity\StandardProduct;
use App\Repository\CategoryRepository;
use App\Repository\CustomProductRepository;
use App\Repository\FillingRepository;
use App\Repository\IcingRepository;
use App\Repository\RequestedProductRepository;
use App\Repository\ShapeRepository;
use App\Repository\StandardProductRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Serializer\Serializer;

class ProductController extends AbstractController
{

    /**
     * @Route("/api/standardProduct", name="addStandardProduct", methods={"post"})
     * @return Response
     */
    public function addStandardProduct(Request $request, CategoryRepository $categoryRepository)
    {
        $data = json_decode($request->getContent(), true);
        $object = new StandardProduct();
        $object->setQteAvailable($data["qteAvailable"]);
        $object->setPicture($data["picture"]);
        $object->setDescription($data["description"]);
        $object->setPrice($data["price"]);
        $object->setText($data["text"]);
        $category = $categoryRepository->find($data["category"]);
        $object->setCategory($category);

        $em = $this->getDoctrine()->getManager();
        $em->persist($object);
        $em->flush();
        $response = new Response('', Response::HTTP_CREATED);
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }

    /**
     * @Route("/api/standardProducts", name="getAllStandardProducts", methods={"GET"})
     */
    public function getAllStandardProducts(StandardProductRepository $repository): Response
    {
        $list = $repository->findAll();
        $encoders = array(new JsonEncoder());
        $serializer = new Serializer([new ObjectNormalizer()], $encoders);
        $data = $serializer->serialize($list, 'json');
        $response = new Response($data, 200);
        //content type
        $response->headers->set('Content-Type', 'application/json');
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }

    /**
     * @param $id
     * @Route("/api/standardProduct/{id}", name="getStandardProduct", methods={"GET"})
     */
    public function getStandardProductById(StandardProductRepository $repository, int $id): Response
    {
        $standardProduct = $repository->find($id);
        $encoders = array(new JsonEncoder());
        $serializer = new Serializer([new ObjectNormalizer()], $encoders);
        $data = $serializer->serialize($standardProduct, 'json');
        $response = new Response($data, 200);
        //content type
        $response->headers->set('Content-Type', 'application/json');
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }


    /**
     * @Route("/api/updateStandardProduct/{id}", name="updateStandardProduct", methods={"put"})
     *
     */
    public function updateStandardProduct(Request $request, $id, CategoryRepository $categoryRepository)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $object = $entityManager->getRepository(StandardProduct::class)->find($id);
        if (!$object) {
            throw $this->createNotFoundException(
                'No product found for id ' . $id
            );
        }
        $data = json_decode($request->getContent(), true);
        $object->setQteAvailable($data["qteAvailable"]);
        $object->setPicture($data["picture"]);
        $object->setDescription($data["description"]);
        $object->setPrice($data["price"]);
        $object->setText($data["text"]);
        $category = $categoryRepository->find($data["category"]);
        $object->setCategory($category);

        $entityManager->flush();
        $response = new Response('', Response::HTTP_OK);
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }
    /**
     * @Route("/api/deleteStandardProduct/{id}", name="deleteStandardProduct", methods={"delete"})
     *
     */
    public function deleteStandardProduct(Request $request, $id, CategoryRepository $categoryRepository)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $object = $entityManager->getRepository(StandardProduct::class)->find($id);
        if (!$object) {
            throw $this->createNotFoundException(
                'No product found for id ' . $id
            );
        }
        $entityManager -> remove($object);
        $entityManager->flush();
        $response = new Response('', Response::HTTP_OK);
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }


    /**
     * @Route("/api/addRequestedProduct", name="addRequestedProduct", methods={"post"})
     * @return Response
     */
    public function addRequestedProduct(Request $request , UserRepository $userRepository)
    {
        $data = json_decode($request->getContent(), true);
        $object = new RequestedProduct();
        $object->setAttachment($data["attachment"]);
        $object->setDescription($data["description"]);
        $object->setPrice($data["price"]);
        $object->setText($data["text"]);
        $user = $userRepository->find($data["user"]);
        $object->setUser($user);

        $em = $this->getDoctrine()->getManager();
        $em->persist($object);
        $em->flush();
        $response = new Response('', Response::HTTP_CREATED);
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }
    /**
     * @Route("/api/requestedProducts", name="getAllRequestedProducts", methods={"GET"})
     */
    public function getAllRequestedProducts(RequestedProductRepository $repository): Response
    {
        $list = $repository->findAll();
        $encoders = array(new JsonEncoder());
        $serializer = new Serializer([new ObjectNormalizer()], $encoders);
        $data = $serializer->serialize($list, 'json');
        $response = new Response($data, 200);
        //content type
        $response->headers->set('Content-Type', 'application/json');
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }



    /**
     * @param $id
     * @Route("/api/requestedProduct/{id}", name="getRequestedProduct", methods={"GET"})
     */
    public function getRequestedProductById(RequestedProductRepository $repository, int $id): Response
    {
        $requestedProduct = $repository->find($id);
        $encoders = array(new JsonEncoder());
        $serializer = new Serializer([new ObjectNormalizer()], $encoders);
        $data = $serializer->serialize($requestedProduct, 'json');
        $response = new Response($data, 200);
        //content type
        $response->headers->set('Content-Type', 'application/json');
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }
    /**
     * @Route("/api/updateRequestedProduct/{id}", name="updateRequestedProduct", methods={"put"})
     *
     */
    public function updateRequestedProduct(Request $request, $id, UserRepository $userRepository)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $object = $entityManager->getRepository(RequestedProduct::class)->find($id);
        if (!$object) {
            throw $this->createNotFoundException(
                'No product found for id ' . $id
            );
        }
        $data = json_decode($request->getContent(), true);
        $object->setAttachment($data["attachment"]);
        $object->setDescription($data["description"]);
        $object->setPrice($data["price"]);
        $object->setText($data["text"]);
        $user = $userRepository->find($data["user"]);
        $object->setUser($user);

        $entityManager->flush();
        $response = new Response('', Response::HTTP_OK);
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }
    /**
     * @Route("/api/deleteRequestedProduct/{id}", name="deleteRequestedProduct", methods={"delete"})
     *
     */
    public function deleteRequestedProduct(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $object = $entityManager->getRepository(RequestedProduct::class)->find($id);
        if (!$object) {
            throw $this->createNotFoundException(
                'No product found for id ' . $id
            );
        }
        $entityManager -> remove($object);
        $entityManager->flush();
        $response = new Response('', Response::HTTP_OK);
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }

    /**
     * @Route("/api/addCustomProduct", name="addCustomProduct", methods={"post"})
     * @return Response
     */
    public function addCustomProduct(Request $request , FillingRepository $fillingRepository,
    IcingRepository $icingRepository, ShapeRepository $shapeRepository)
    {
        $data = json_decode($request->getContent(), true);
        $object = new CustomProduct();
        $object->setPrice($data["price"]);
        $object->setText($data["text"]);
        $object->setNbLayers($data["nbLayers"]);
        $filling = $fillingRepository->find($data["filling"]);
        $icing = $icingRepository->find($data["icing"]);
        $shape = $shapeRepository->find($data["shape"]);

        $object->setFilling($filling);
        $object->setIcing($icing);
        $object->setShape($shape);

        $em = $this->getDoctrine()->getManager();
        $em->persist($object);
        $em->flush();
        $response = new Response('', Response::HTTP_CREATED);
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }

    /**
     * @Route("/api/customProducts", name="getAllCustomProducts", methods={"GET"})
     */
    public function getAllCustomProducts(CustomProductRepository $repository): Response
    {
        $list = $repository->findAll();
        $encoders = array(new JsonEncoder());
        $serializer = new Serializer([new ObjectNormalizer()], $encoders);
        $data = $serializer->serialize($list, 'json');
        $response = new Response($data, 200);
        //content type
        $response->headers->set('Content-Type', 'application/json');
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }

    /**
     * @param $id
     * @Route("/api/customProduct/{id}", name="getCustomProductById", methods={"GET"})
     */
    public function getCustomProductById(CustomProductRepository $repository, int $id): Response
    {
        $requestedProduct = $repository->find($id);
        $encoders = array(new JsonEncoder());
        $serializer = new Serializer([new ObjectNormalizer()], $encoders);
        $data = $serializer->serialize($requestedProduct, 'json');
        $response = new Response($data, 200);
        //content type
        $response->headers->set('Content-Type', 'application/json');
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }

    /**
     * @Route("/api/updateCustomProduct/{id}", name="updateCustomProduct", methods={"put"})
     *
     */
    public function updateCustomProduct(Request $request, $id, FillingRepository $fillingRepository,
                                        IcingRepository $icingRepository, ShapeRepository $shapeRepository)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $object = $entityManager->getRepository(CustomProduct::class)->find($id);
        if (!$object) {
            throw $this->createNotFoundException(
                'No product found for id ' . $id
            );
        }
        $data = json_decode($request->getContent(), true);
        $object->setPrice($data["price"]);
        $object->setText($data["text"]);
        $object->setNbLayers($data["nbLayers"]);
        $filling = $fillingRepository->find($data["filling"]);
        $icing = $icingRepository->find($data["icing"]);
        $shape = $shapeRepository->find($data["shape"]);

        $object->setFilling($filling);
        $object->setIcing($icing);
        $object->setShape($shape);


        $entityManager->flush();
        $response = new Response('', Response::HTTP_OK);
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }

    /**
     * @Route("/api/deleteCustomProduct/{id}", name="deleteCustomProduct", methods={"delete"})
     *
     */
    public function deleteCustomProduct(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $object = $entityManager->getRepository(CustomProduct::class)->find($id);
        if (!$object) {
            throw $this->createNotFoundException(
                'No product found for id ' . $id
            );
        }
        $entityManager -> remove($object);
        $entityManager->flush();
        $response = new Response('', Response::HTTP_OK);
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }


    /**
     * @Route("/api/addFilling", name="addFilling", methods={"post"})
     * @return Response
     */
    public function addFilling(Request $request )
    {
        $data = json_decode($request->getContent(), true);
        $object = new Filling();
        $object->setLabel($data["label"]);
        $object->setPic($data["pic"]);


        $em = $this->getDoctrine()->getManager();
        $em->persist($object);
        $em->flush();
        $response = new Response('', Response::HTTP_CREATED);
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }

    /**
     * @Route("/api/getAllFillings", name="getAllFillings", methods={"GET"})
     */
    public function getAllFillings(FillingRepository $repository): Response
    {
        $list = $repository->findAll();
        $encoders = array(new JsonEncoder());
        $serializer = new Serializer([new ObjectNormalizer()], $encoders);
        $data = $serializer->serialize($list, 'json');
        $response = new Response($data, 200);
        //content type
        $response->headers->set('Content-Type', 'application/json');
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }

    /**
     * @param $id
     * @Route("/api/getFillingById/{id}", name="getFillingById", methods={"GET"})
     */
    public function getFillingById(FillingRepository $repository, int $id): Response
    {
        $filling = $repository->find($id);
        $encoders = array(new JsonEncoder());
        $serializer = new Serializer([new ObjectNormalizer()], $encoders);
        $data = $serializer->serialize($filling, 'json');
        $response = new Response($data, 200);
        //content type
        $response->headers->set('Content-Type', 'application/json');
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }

    /**
     * @Route("/api/updateFilling/{id}", name="updateFilling", methods={"put"})
     *
     */
    public function updateFilling(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $object = $entityManager->getRepository(Filling::class)->find($id);
        if (!$object) {
            throw $this->createNotFoundException(
                'No filling found for id ' . $id
            );
        }
        $data = json_decode($request->getContent(), true);
        $object->setLabel($data["label"]);
        $object->setPic($data["pic"]);


        $entityManager->flush();
        $response = new Response('', Response::HTTP_OK);
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }

    /**
     * @Route("/api/deleteFilling/{id}", name="deleteFilling", methods={"delete"})
     *
     */
    public function deleteFilling(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $object = $entityManager->getRepository(Filling::class)->find($id);
        if (!$object) {
            throw $this->createNotFoundException(
                'No filling found for id ' . $id
            );
        }
        $entityManager -> remove($object);
        $entityManager->flush();
        $response = new Response('', Response::HTTP_OK);
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }

    /**
     * @Route("/api/addIcing", name="addIcing", methods={"post"})
     * @return Response
     */
    public function addIcing(Request $request )
    {
        $data = json_decode($request->getContent(), true);
        $object = new Icing();
        $object->setLabel($data["label"]);
        $object->setPic($data["pic"]);


        $em = $this->getDoctrine()->getManager();
        $em->persist($object);
        $em->flush();
        $response = new Response('', Response::HTTP_CREATED);
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }

    /**
     * @Route("/api/getAllIcings", name="getAllIcings", methods={"GET"})
     */
    public function getAllIcings(IcingRepository $repository): Response
    {
        $list = $repository->findAll();
        $encoders = array(new JsonEncoder());
        $serializer = new Serializer([new ObjectNormalizer()], $encoders);
        $data = $serializer->serialize($list, 'json');
        $response = new Response($data, 200);
        //content type
        $response->headers->set('Content-Type', 'application/json');
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }

    /**
     * @param $id
     * @Route("/api/getIcingById/{id}", name="getIcingById", methods={"GET"})
     */
    public function getIcingById(IcingRepository $repository, int $id): Response
    {
        $icing = $repository->find($id);
        $encoders = array(new JsonEncoder());
        $serializer = new Serializer([new ObjectNormalizer()], $encoders);
        $data = $serializer->serialize($icing, 'json');
        $response = new Response($data, 200);
        //content type
        $response->headers->set('Content-Type', 'application/json');
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }

    /**
     * @Route("/api/updateIcing/{id}", name="updateIcing", methods={"put"})
     *
     */
    public function updateIcing(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $object = $entityManager->getRepository(Icing::class)->find($id);
        if (!$object) {
            throw $this->createNotFoundException(
                'No Icing found for id ' . $id
            );
        }
        $data = json_decode($request->getContent(), true);
        $object->setLabel($data["label"]);
        $object->setPic($data["pic"]);


        $entityManager->flush();
        $response = new Response('', Response::HTTP_OK);
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }

    /**
     * @Route("/api/deleteIcing/{id}", name="deleteIcing", methods={"delete"})
     *
     */
    public function deleteIcing(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $object = $entityManager->getRepository(Icing::class)->find($id);
        if (!$object) {
            throw $this->createNotFoundException(
                'No Icing found for id ' . $id
            );
        }
        $entityManager -> remove($object);
        $entityManager->flush();
        $response = new Response('', Response::HTTP_OK);
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }


    /**
     * @Route("/api/addShape", name="addShape", methods={"post"})
     * @return Response
     */
    public function addShape(Request $request )
    {
        $data = json_decode($request->getContent(), true);
        $object = new Shape();
        $object->setLabel($data["label"]);
        $object->setPic($data["pic"]);


        $em = $this->getDoctrine()->getManager();
        $em->persist($object);
        $em->flush();
        $response = new Response('', Response::HTTP_CREATED);
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }

    /**
     * @Route("/api/getAllShapes", name="getAllShapes", methods={"GET"})
     */
    public function getAllShapes(ShapeRepository $repository): Response
    {
        $list = $repository->findAll();
        $encoders = array(new JsonEncoder());
        $serializer = new Serializer([new ObjectNormalizer()], $encoders);
        $data = $serializer->serialize($list, 'json');
        $response = new Response($data, 200);
        //content type
        $response->headers->set('Content-Type', 'application/json');
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }

    /**
     * @param $id
     * @Route("/api/getShapeById/{id}", name="getShapeById", methods={"GET"})
     */
    public function getShapeById(ShapeRepository $repository, int $id): Response
    {
        $shape = $repository->find($id);
        $encoders = array(new JsonEncoder());
        $serializer = new Serializer([new ObjectNormalizer()], $encoders);
        $data = $serializer->serialize($shape, 'json');
        $response = new Response($data, 200);
        //content type
        $response->headers->set('Content-Type', 'application/json');
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }

    /**
     * @Route("/api/updateShape/{id}", name="updateShape", methods={"put"})
     *
     */
    public function updateShape(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $object = $entityManager->getRepository(Shape::class)->find($id);
        if (!$object) {
            throw $this->createNotFoundException(
                'No Shape found for id ' . $id
            );
        }
        $data = json_decode($request->getContent(), true);
        $object->setLabel($data["label"]);
        $object->setPic($data["pic"]);


        $entityManager->flush();
        $response = new Response('', Response::HTTP_OK);
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }

    /**
     * @Route("/api/deleteShape/{id}", name="deleteShape", methods={"delete"})
     *
     */
    public function deleteShape(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $object = $entityManager->getRepository(Shape::class)->find($id);
        if (!$object) {
            throw $this->createNotFoundException(
                'No Shape found for id ' . $id
            );
        }
        $entityManager -> remove($object);
        $entityManager->flush();
        $response = new Response('', Response::HTTP_OK);
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }

    /**
     * @Route("/api/getAllCategories", name="getAllCategories", methods={"GET"})
     */
    public function getAllCategories(CategoryRepository $repository): Response
    {
        $list = $repository->findAll();
        $encoders = array(new JsonEncoder());
        $serializer = new Serializer([new ObjectNormalizer()], $encoders);
        $data = $serializer->serialize($list, 'json');
        $response = new Response($data, 200);
        //content type
        $response->headers->set('Content-Type', 'application/json');
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }

    /**
     * @param $id
     * @Route("/api/getCategory/{id}", name="getCategoryById", methods={"GET"})
     */
    public function getCategoryById(CategoryRepository $repository, int $id): Response
    {
        $shape = $repository->find($id);
        $encoders = array(new JsonEncoder());
        $serializer = new Serializer([new ObjectNormalizer()], $encoders);
        $data = $serializer->serialize($shape, 'json');
        $response = new Response($data, 200);
        //content type
        $response->headers->set('Content-Type', 'application/json');
        //Allow all websites
        $response->headers->set('Access-Control-Allow-Origin', '*');
        // You can set the allowed methods too, if you want
        $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }


}
