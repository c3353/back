<?php

namespace App\Entity;

use App\Repository\StandardProductRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StandardProductRepository::class)
 */
class StandardProduct extends Product
{


    /**
     * @ORM\Column(type="integer")
     */
    private $qteAvailable;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $picture;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="standardProducts")
     * @ORM\JoinColumn(nullable=true)
     */
    private $category;



    public function getQteAvailable(): ?int
    {
        return $this->qteAvailable;
    }

    public function setQteAvailable(int $qteAvailable): self
    {
        $this->qteAvailable = $qteAvailable;

        return $this;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }
}
