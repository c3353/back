<?php

namespace App\Entity;

use App\Repository\CustomProductRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CustomProductRepository::class)
 */
class CustomProduct extends Product
{

    /**
     * @ORM\Column(type="integer")
     */
    private $nbLayers;

    /**
     * @ORM\ManyToOne(targetEntity=Shape::class)
     * @ORM\JoinColumn(nullable=true)
     */
    private $shape;

    /**
     * @ORM\ManyToOne(targetEntity=Filling::class)
     * @ORM\JoinColumn(nullable=true)
     */
    private $filling;

    /**
     * @ORM\ManyToOne(targetEntity=Icing::class)
     * @ORM\JoinColumn(nullable=true)
     */
    private $icing;


    public function getNbLayers(): ?int
    {
        return $this->nbLayers;
    }

    public function setNbLayers(int $nbLayers): self
    {
        $this->nbLayers = $nbLayers;

        return $this;
    }

    public function getShape(): ?Shape
    {
        return $this->shape;
    }

    public function setShape(?Shape $shape): self
    {
        $this->shape = $shape;

        return $this;
    }

    public function getFilling(): ?Filling
    {
        return $this->filling;
    }

    public function setFilling(?Filling $filling): self
    {
        $this->filling = $filling;

        return $this;
    }

    public function getIcing(): ?Icing
    {
        return $this->icing;
    }

    public function setIcing(?Icing $icing): self
    {
        $this->icing = $icing;

        return $this;
    }
}
