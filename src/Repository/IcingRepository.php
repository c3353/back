<?php

namespace App\Repository;

use App\Entity\Icing;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Icing|null find($id, $lockMode = null, $lockVersion = null)
 * @method Icing|null findOneBy(array $criteria, array $orderBy = null)
 * @method Icing[]    findAll()
 * @method Icing[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IcingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Icing::class);
    }

    // /**
    //  * @return Icing[] Returns an array of Icing objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Icing
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
