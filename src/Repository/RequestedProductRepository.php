<?php

namespace App\Repository;

use App\Entity\RequestedProduct;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RequestedProduct|null find($id, $lockMode = null, $lockVersion = null)
 * @method RequestedProduct|null findOneBy(array $criteria, array $orderBy = null)
 * @method RequestedProduct[]    findAll()
 * @method RequestedProduct[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RequestedProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RequestedProduct::class);
    }

    // /**
    //  * @return RequestedProduct[] Returns an array of RequestedProduct objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RequestedProduct
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
