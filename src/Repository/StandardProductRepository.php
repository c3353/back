<?php

namespace App\Repository;

use App\Entity\StandardProduct;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StandardProduct|null find($id, $lockMode = null, $lockVersion = null)
 * @method StandardProduct|null findOneBy(array $criteria, array $orderBy = null)
 * @method StandardProduct[]    findAll()
 * @method StandardProduct[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StandardProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StandardProduct::class);
    }

    // /**
    //  * @return StandardProduct[] Returns an array of StandardProduct objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StandardProduct
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
