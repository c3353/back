<?php

namespace App\Repository;

use App\Entity\Filling;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Filling|null find($id, $lockMode = null, $lockVersion = null)
 * @method Filling|null findOneBy(array $criteria, array $orderBy = null)
 * @method Filling[]    findAll()
 * @method Filling[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FillingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Filling::class);
    }

    // /**
    //  * @return Filling[] Returns an array of Filling objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Filling
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
